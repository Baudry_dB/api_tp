const express = require('express');
const _ = require('lodash');

const router = express.Router();


//creation tableau de donnees

/*
let movies= [{
    id: String,
    movie: String,
    yearOfRelease: Number,
    duration: Number, // en minutes,
    actors: [String, String],
    poster: String, // lien vers une image d'affiche,
    boxOffice: Number, // en USD$,
    rottenTomatoesScore: Number
   }];
  */
 
  let movies= [{
    id: "0",
    movie: "Pearl Harbor",
    actor: "Tom Hanks"
    //yearOfRelease: Number,
   // duration: Number, // en minutes,
   // actors: [String, String],
   // poster: String, // lien vers une image d'affiche,
   // boxOffice: Number, // en USD$,
   // rottenTomatoesScore: Number
   }];


/* GET movies listing. */
/*
router.get('/', function(req, res, next) {

  res.send('respond with a resource');
    
});
*/

/* GET movies listing. */
router.get('/', (req, res) => {
   
    // Get List of movie and return JSON
    res.status(200).json({ movies });
  });
  

/* GET one movie. */
router.get('/:id', (req, res) => {
    const { id } = req.params;
    // Find movie in DB
    const movie = _.find(movies, ["id", id]);
    // Return movie
    res.status(200).json({
      message: 'Movie found!',
      movie 
    });
  });

  /* PUT new movie. */
router.put('/', (req, res) => {
    // Get the data from request 
    const { movie } = req.body;
    const { actor } = req.body;
    // Create new unique id
    const id = _.uniqueId();
    // Insert it in array (normaly with connect the data with the database)
    movies.push({ movie, actor, id });
    // Return message
    res.json({
      message: `Just added ${id}`,
      movie: { movie, id },
      actor: { actor, id }
    });
  });
  


/* DELETE movie. */
router.delete('/:id', (req, res) => {
    // Get the :id of the movie we want to delete from the params of the request
    const { id } = req.params;
  
    // Remove from "DB"
    _.remove(movies, ["id", id]);
  
    // Return message
    res.json({
      message: `Just removed ${id}`
    });
  });

  /* UPDATE movie. */
router.post('/:id', (req, res) => {
    // Get the :id of the movie we want to update from the params of the request
    const { id } = req.params;
    // Get the new data of the movie we want to update from the body of the request
    const { movie } = req.body;
    const { actor } = req.body;
    // Find in DB
    const movieToUpdate = _.find(movies, ["id", id]);
    const actorToUpdate = _.find(movies, ["id", id]); //faut il garder cela?
    // Update data with new data (js is by address)
    movieToUpdate.movie = movie;
    actorToUpdate.actor = actor;
  
    // Return message
    res.json({
      message: `Just updated ${id} with ${movie} and  ${actor}`
    });
  });
  


module.exports = router;
